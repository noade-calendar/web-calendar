const fs = require('fs');

let template = `
export const environment = {
    production: PROD,
    firebase : {
        FIREBASE
    }
  };
`

template = template.replace("FIREBASE", process.argv[2]);
fs.writeFileSync('src/environments/environment.prod.ts', template.replace("PROD", "true"));
fs.writeFileSync('src/environments/environment.ts', template.replace("PROD", "false"));
