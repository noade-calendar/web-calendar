import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { FullCalendarModule } from '@fullcalendar/angular';

import { WebStorageModule } from 'ngx-store';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

// COMPONENTS
import { FooterComponent } from '@comp/footer/footer.component';
import { InfoModalComponent } from '@comp/info-modal/info-modal.component';
import { Groups3aModalComponent } from './components/groups3a-modal/groups3a-modal.component';

// PAGES
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { EdtPageComponent } from './pages/edt-page/edt-page.component';
import { PickPageComponent } from './pages/pick-page/pick-page.component';

import { EdtService } from '@services/edt.service';

import { environment } from '../environments/environment';
import { NavbarModule } from '@comp/navbar/navbar.component.module';
import { Groups3aModalModule } from '@comp/groups3a-modal/groups3a-modal.component.module';
import { InfoModalModule } from '@comp/info-modal/info-modal.component.module';
import { FormModule } from '@comp/form/form.component.module';
import { AnalyticsService } from '@services/analytics.service';
import { MatProgressSpinnerModule, MatExpansionModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    PageNotFoundComponent,
    EdtPageComponent,
    PickPageComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FullCalendarModule,
    WebStorageModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatToolbarModule,
    MatDividerModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    NavbarModule,
    Groups3aModalModule,
    InfoModalModule,
    FormModule,
    MatExpansionModule
  ],
  entryComponents: [
    InfoModalComponent,
    Groups3aModalComponent
  ],
  providers: [EdtService, AnalyticsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
