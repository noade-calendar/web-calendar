import { TestBed, inject } from '@angular/core/testing';

import { EdtService } from './edt.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';

describe('EdtService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        EdtService,
        {
          provide: AngularFirestore, useValue: {
            collection: (_col: string) => {
              return {
                doc: (_doc: string) => {
                }
              };
            }
          }
        }
      ]
    });
  });

  it('should be created', inject([EdtService], (service: EdtService) => {
    expect(service).toBeTruthy();
  }));
});
