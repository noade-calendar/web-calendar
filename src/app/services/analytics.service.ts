import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-store';

import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';

import * as firebase from 'firebase/app';
import { isEmpty } from 'lodash';


@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {


  @LocalStorage() lastVisited: Date = null;
  @LocalStorage() dailyCalendars: string[] = [];
  analyticsCollection: AngularFirestoreCollection;
  dayDoc: AngularFirestoreDocument;

  constructor(fs: AngularFirestore) {
    this.analyticsCollection = fs.collection('analytics');
  }

  async newDailyVisit() {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    const date = `${today.getDate()}-${today.getMonth()}-${today.getFullYear()}`;
    this.dayDoc = this.analyticsCollection.doc(date);
    if (!this.lastVisited || new Date(this.lastVisited) < today) {
      await this.dayDoc.set({
        count: firebase.firestore.FieldValue.increment(1)
      }, { merge: true });
      this.lastVisited = today;
      this.dailyCalendars = [];

    }
  }

  async pushCalendars(calIds: string[]) {
    await this.newDailyVisit();
    const dict: Object = calIds.filter(
      c => !this.dailyCalendars.includes(c)
    ).reduce(
      (d, id) => (d[id] = firebase.firestore.FieldValue.increment(1), d), {}
    );
    if (!isEmpty(dict)) {
      await this.dayDoc.set({
        calendars: { ...dict }
      }, { merge: true });
      this.dailyCalendars = [...new Set(calIds.concat(this.dailyCalendars))];
    }
  }

}
