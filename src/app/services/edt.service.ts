import { Injectable } from '@angular/core';
import { EventInput } from '@fullcalendar/core';
import { map, take } from 'rxjs/operators';
import * as ColorHash from 'color-hash';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { LocalStorage } from 'ngx-store';
import { Subject } from 'rxjs';
export interface Calendar {
  ids: string[];
  name: string;
  offline: boolean;
}

interface FirestoreEvents {
  [uid: string]: {
    description: string;
    end: any;
    start: any;
    location: string;
    summary: string;
  };
}

@Injectable()
export class EdtService {

  @LocalStorage() selected3aGroup: string = null;
  calendarIds: string[] = [];

  calendars: AngularFirestoreCollection<{}>;

  refresher: Subject<{}>;

  resourcesDoc: AngularFirestoreDocument;
  resources: Object;

  groups3a = [
    { name: '', value: null },
    { name: 'Aquarius (1)', value: 'Aquarius' },
    { name: 'Aries (2)', value: 'Aries' },
    { name: 'Centaurus (3)', value: 'Centaurus' },
    { name: 'Draco (4)', value: 'Draco' },
    { name: 'Gemini (5)', value: 'Gemini' },
    { name: 'Hercules (7)', value: 'Hercules' },
    { name: 'Hydra (6)', value: 'Hydra' },
    { name: 'Leo (10)', value: 'Leo' },
    { name: 'Libra (8)', value: 'Libra' },
    { name: 'Musca (9)', value: 'Musca' },
    { name: 'Norma (12)', value: 'Norma' },
    { name: 'Orion (11)', value: 'Orion' },
    { name: 'Pegasus (13)', value: 'Pegasus' },
    { name: 'Phoenix (14)', value: 'Phoenix' },
    { name: 'Sagittarius (15)', value: 'Sagittarius' },
    { name: 'Scorpius (16)', value: 'Scorpius' },
    { name: 'Taurus (17)', value: 'Taurus' },
    { name: 'Volans (18)', value: 'Volans' }
  ];

  constructor(fs: AngularFirestore) {
    this.calendars = fs.collection('calendars');
    this.resourcesDoc = fs.collection('calendars').doc('resources');
    this.refresher = new Subject();
  }

  /**
   * @param  {string} id - Calendar id
   * @returns Promise containing queried events from specified calendar
   */
  loadCalendar(id: string): Promise<{ events: EventInput[], name: string, updated: Date }> {
    this.calendarIds.push(id);
    return this.calendars.doc(id).valueChanges()
      .pipe(
        map(val => {
          if (!val) { throw new Error(`Calendar ${id} does not exist`); }
          return {
            events: this.mapToFullCalendar(val['events']),
            name: val['name'],
            updated: val['updated'].toDate()
          };
        }),
        take(1)
      ).toPromise();
  }
  // makes events component into correct fullcalendar EventInput array
  /**
   * Converts Firestore events to FullCalendar events
   * @param  {FirestoreEvents} events - Firestore dictionary of events
   * @returns EventInput[]
   */
  mapToFullCalendar(events: FirestoreEvents): EventInput[] {
    const colorHash = new ColorHash();
    const filtered = this.selected3aGroup ? Object.keys(events).filter(
      key => {
        const descr = events[key].description;
        return !descr.includes('TC3') ||
          descr.includes('TC3') && descr.includes(this.selected3aGroup);
      }
    ) : Object.keys(events);
    return filtered.map(key => {
      const event = events[key];
      return {
        description: event.description,
        location: event.location,
        start: event.start.toDate(),
        end: event.end.toDate(),
        id: key,
        title: event.summary + '\n \n' + event.location,
        trueTitle: event.summary,
        color: colorHash.hex(event.summary)
      };
    });
  }

  async loadResources(): Promise<Object> {
    this.resources = await this.resourcesDoc
      .valueChanges().pipe(
        map(val => {
          if (!val) {
            throw new Error('No resources');
          } else {
            return val;
          }
        }),
        take(1)
      ).toPromise();
    return this.resources;
  }

  resetIds() {
    this.calendarIds = [];
  }

  set3aGroup(group: string) {
    this.selected3aGroup = group;
  }

  refreshCalendar() {
    this.refresher.next();
  }

}
