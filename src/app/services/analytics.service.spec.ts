import { TestBed, inject } from '@angular/core/testing';

import { AnalyticsService } from './analytics.service';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

describe('AnalyticsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AnalyticsService,
        {
          provide: AngularFirestore, useValue: {
            collection: (_col: string) => {
              return {
                doc: (_doc: string) => {
                }
              };
            }
          }
        }
      ]
    });
  });

  it('should be created', inject([AnalyticsService], (service: AnalyticsService) => {
    expect(service).toBeTruthy();
  }));
});
