import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { EdtService } from '@services/edt.service';

@Component({
  selector: 'app-groups3a-modal',
  templateUrl: './groups3a-modal.component.html',
  styleUrls: ['./groups3a-modal.component.scss']
})
export class Groups3aModalComponent implements OnInit {

  selectedGroup: string;
  groups = [];

  constructor(
    private edt: EdtService,
    public dialogRef: MatDialogRef<Groups3aModalComponent>) { }

  ngOnInit() {
    this.selectedGroup = this.edt.selected3aGroup;
    this.groups = this.edt.groups3a;
  }

  save() {
    this.edt.set3aGroup(this.selectedGroup);
    this.edt.refreshCalendar();
    this.dialogRef.close();
  }

}
