import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { Groups3aModalComponent } from './groups3a-modal.component';
import { Groups3aModalModule } from './groups3a-modal.component.module';
import { EdtService } from '@services/edt.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('Groups3aModalComponent', () => {
  let component: Groups3aModalComponent;
  let fixture: ComponentFixture<Groups3aModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        Groups3aModalModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: EdtService, useValue: {} },
        {
          provide: MatDialogRef,
          useValue: {
            close: (dialogResult: any) => { }
          }
        },
        { provide: MAT_DIALOG_DATA, useValue: [] }
      ]
    }).compileComponents();
  }));

  beforeEach(async(
    inject(
      [EdtService, MatDialogRef],
      (edt: EdtService, dialog: MatDialogRef<Groups3aModalComponent>) => {
        fixture = TestBed.createComponent(Groups3aModalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      })));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
