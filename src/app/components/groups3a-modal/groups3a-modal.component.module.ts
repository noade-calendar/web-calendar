import { NgModule } from '@angular/core';
import { MatSelectModule, MatOptionModule, MatButtonModule, MatFormFieldModule, MatDialogModule } from '@angular/material';
import { Groups3aModalComponent } from './groups3a-modal.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    Groups3aModalComponent
  ],
  exports: [
    Groups3aModalComponent
  ],
  imports: [
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    CommonModule,
    MatDialogModule
  ],
})
export class Groups3aModalModule { }
