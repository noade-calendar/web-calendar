import { Component, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Groups3aModalComponent } from '@comp/groups3a-modal/groups3a-modal.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  @Output() toggle = new EventEmitter<void>();

  constructor(private dialog: MatDialog) { }

  select3aGroups() {
    this.dialog.open(Groups3aModalComponent);
  }

}
