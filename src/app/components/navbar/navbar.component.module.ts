import { NgModule } from '@angular/core';

import { MatDialogModule, MatIconModule, MatDividerModule, MatToolbarModule, MatButtonModule } from '@angular/material';
import { NavbarComponent } from './navbar.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    NavbarComponent
  ],
  exports: [
    NavbarComponent
  ],
  imports: [
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatDividerModule,
    MatButtonModule,
    RouterModule
  ],
})
export class NavbarModule { }
