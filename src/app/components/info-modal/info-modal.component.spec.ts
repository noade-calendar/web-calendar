import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { InfoModalModule } from './info-modal.component.module';
import { InfoModalComponent } from './info-modal.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

describe('InfoModalComponent', () => {
  let component: InfoModalComponent;
  let fixture: ComponentFixture<InfoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [InfoModalModule],
      providers: [{
        provide: MatDialogRef,
        useValue: {
          close: (dialogResult: any) => { }
        }
      },
      { provide: MAT_DIALOG_DATA, useValue: [] }, ]
    }).compileComponents();
  }));

  beforeEach(async(inject([MatDialogRef], (dialog: MatDialogRef<InfoModalComponent>) => {
    fixture = TestBed.createComponent(InfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  })));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
