import { NgModule } from '@angular/core';

import { InfoModalComponent } from './info-modal.component';
import { MatDialogModule } from '@angular/material';

@NgModule({
  declarations: [
    InfoModalComponent
  ],
  exports: [
    InfoModalComponent
  ],
  imports: [
    MatDialogModule,
  ],
})
export class InfoModalModule { }
