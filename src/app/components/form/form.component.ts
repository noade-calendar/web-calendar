import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  objectKeys = Object.keys;
  selectedClass: string;
  selectedYear: string;
  selectedGroups: string[];

  @Input() resources: Object = [];
  @Output() selected = new EventEmitter<string[]>();

  constructor() {
  }

  emit() {
    const year = (this.resources[this.selectedClass] || null)[this.selectedYear] || null;
    if (!year) { return; }
    const ids = this.selectedGroups.map(group => year[group]);
    this.selected.emit(ids);
  }
}
