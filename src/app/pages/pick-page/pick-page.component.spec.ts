import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { PickPageComponent } from './pick-page.component';
import { FormModule } from '@comp/form/form.component.module';
import { EdtService } from '@services/edt.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('PickPageComponent', () => {
  let component: PickPageComponent;
  let fixture: ComponentFixture<PickPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PickPageComponent,

      ],
      imports: [
        RouterTestingModule,
        FormModule
      ],
      providers: [
        { provide: EdtService, useValue: {} },
      ]
    }).compileComponents();
  }));

  beforeEach(async(
    inject([EdtService], (edt: EdtService) => {
      fixture = TestBed.createComponent(PickPageComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    })));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
