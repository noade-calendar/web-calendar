import { Component, OnInit } from '@angular/core';
import { EdtService } from '@services/edt.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pick-page',
  templateUrl: './pick-page.component.html',
  styleUrls: ['./pick-page.component.scss']
})
export class PickPageComponent implements OnInit {

  resources: Object = null;
  error = false;

  constructor(private serv: EdtService, private router: Router) { }

  async ngOnInit() {
    try {
      this.resources = !this.serv.resources ? await this.serv.loadResources() : this.serv.resources;
    } catch (err) {
      this.error = true;
      console.error(err);
    }
  }

  toCalendars(ids: string[]) {
    this.router.navigate(['/edt', { ids: ids }]);
  }
}
