import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { EdtPageComponent } from './edt-page.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { EdtService } from '@services/edt.service';
import { MatDialogModule, MatProgressSpinnerModule, MatCardModule, MatExpansionModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { AnalyticsService } from '@services/analytics.service';

describe('EdtPageComponent', () => {
  let component: EdtPageComponent;
  let fixture: ComponentFixture<EdtPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EdtPageComponent],
      imports: [
        FullCalendarModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatCardModule,
        MatExpansionModule,
        RouterTestingModule.withRoutes([
          { path: 'pick', component: EdtPageComponent }
        ])
      ],
      providers: [
        {
          provide: EdtService, useValue: {
            refresher: new Observable(),
            calendarIds: []
          }
        },
        {
          provide: AnalyticsService, useValue: {}
        },
      ]
    }).compileComponents();
  }));

  beforeEach(async(
    inject([EdtService, AnalyticsService],
      () => {
        fixture = TestBed.createComponent(EdtPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      })));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
