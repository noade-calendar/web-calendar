import { AnalyticsService } from '@services/analytics.service';
import { EdtService } from '@services/edt.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { EventInput } from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { MatDialog } from '@angular/material/dialog';

import { InfoModalComponent } from '@comp/info-modal/info-modal.component';
import { unionWith } from 'lodash';


@Component({
  selector: 'app-edt-page',
  templateUrl: './edt-page.component.html',
  styleUrls: ['./edt-page.component.scss']
})
export class EdtPageComponent implements OnInit {

  calendarPlugins = [timeGridPlugin];
  @ViewChild('calendar', { static: false }) calendarComponent: FullCalendarComponent;
  events: EventInput[] = [];

  calInfos: {name: string, updated: Date}[] = [];

  error = false;

  ids = [];

  constructor(
    private edt: EdtService,
    private analytics: AnalyticsService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const routeIds = this.route.snapshot.paramMap.has('ids') ? this.route.snapshot.paramMap.get('ids').split(',') : [];
    if (routeIds.length === 0) {
      this.router.navigate(['/pick']);
    }
    this.initLoading();
    this.route.paramMap.subscribe(params => {
      const ids: string[] = params.has('ids') ? params.get('ids').split(',') : [];
      this.ids = ids;
      this.loadCalendars(ids);
    });
    this.edt.refresher.subscribe(() => {
      this.loadCalendars(this.ids);
    });
  }

  /**
   * Load all specified calendars into FullCalendar component events model
   * @param  {string[]} ids - Array of calendar ids to retrieve events from
   */
  async loadCalendars(ids: string[]) {
    if (ids.length === 0) { return; }
    try {
      this.events = [];
      this.calInfos = [];
      this.edt.resetIds();
      const calendarPromises = ids.map(id => this.edt.loadCalendar(id));
      for (const cp of calendarPromises) {
        try {
          const {events, ...infos} = await cp;
          this.events = unionWith(this.events, events, (a, b) => a['id'] === b['id']);
          this.calInfos.push(infos);
        } catch (error) {
          console.error(error);
        }
      }
      if (this.events.length === 0) {
        throw new Error('No calendar succesfully loaded');
      }
      await this.analytics.pushCalendars(ids);
      this.calInfos.sort((a, b) => a['updated'].getTime() - b['updated'].getTime());
    } catch (error) {
      console.error(error);
      this.error = true;
    }
  }

  openInfo(ev) {
    this.dialog.open(InfoModalComponent, {
      data: {
        title: ev.event['extendedProps']['trueTitle'],
        description: ev.event['extendedProps']['description'],
        location: ev.event['extendedProps']['location']
      }
    });
  }

  initLoading() {
    // Load saved calendars first
    // this.loadCalendars(this.edt.calendarIds);
  }
}
